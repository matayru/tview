from bitmex import trade, get_current_position
from gmail import get_alert, mark_msg_as_read
from threading import Thread
from setting import TRADE_SIZE_PERCENT as T, LEVERAGE as L, INTERVAL as I
import time
import logging
import signal

# Filter google oauth log messages
l2 = logging.getLogger('googleapiclient.discovery')
l2.setLevel(logging.CRITICAL)

# Main application logging
# Set cronjob to rotate logfile or send logs to rsyslog
logging.basicConfig(level=logging.INFO)
hdlr = logging.FileHandler('bitmex.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger = logging.getLogger('BITMEX')
logger.addHandler(hdlr) 
logger.info('Started Successfully with settings:')
logger.info(f'size:{T}, leverage:{L}, interval:{I}')


def monitor():
    while True:
        alert = get_alert()
        if not alert:
            pass
        elif alert == get_current_position():
            mark_msg_as_read()
        else:
            confirmation = trade(alert)
            if not confirmation:
                pass
            elif len(confirmation) == 1:
                logger.info('EVENT: ====== STRICT CLOSE')
                logger.info('PREVIOUS POSITION CLOSED with confirmation:')
                logger.info(confirmation['close'])
            elif len(confirmation) == 2:
                logger.info('EVENT: ====== SMART CLOSE')
                logger.info('PREVIOUS POSITION CLOSED with confirmation:')
                logger.info(confirmation['close'])
                logger.info('NEW POSITION OPEN, with confirmation:')
                logger.info(confirmation['open'])
            elif len(confirmation) == 3:
                logger.info('EVENT: ====== STRICT OPEN')
                logger.info('NEW POSITION OPEN, with confirmation:')
                logger.info(confirmation['open'])

        time.sleep(I)


if __name__ == '__main__':
        try:
            t = Thread(target=monitor, daemon=True)
            t.start()
            t.join()
            signal.pause()
        except KeyboardInterrupt:
            logger.info('SHUTTING DOWN')
