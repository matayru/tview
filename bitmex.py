import os
import hashlib
import hmac
from urllib.parse import quote
from json import load, dumps
import requests
import pendulum
from gmail import get_alert, mark_msg_as_read
from setting import (API_ID, API_SEC, URL, TRADE_SIZE_PERCENT,
                     LEVERAGE, alert_bin)


def gen_sig(fn):
    '''
    A decorator that generates signatures for Bitmex API GET & POST requests
    '''
    def wrapper(path, data, expires, verb):
        message = (verb + path + expires + data).encode()
        sig = hmac.new(API_SEC.encode('utf-8'), message,
                       digestmod=hashlib.sha256).hexdigest()
        headers = {'api-expires': expires,
                   'api-signature': sig,
                   'api-key': API_ID,
                   'Content-Type': 'application/json'}
        return fn(path, data, expires, verb, headers)
    return wrapper


@gen_sig
def get_(path, data, expires, verb, headers):
    '''
    sends get request to Bitmex API.
    :param path: path to API resource.
    :param headers: is set by the wrapper function.
    :param data: for sending data to server.
    '''
    try:
        r = requests.get(URL + f'{path}', headers=headers).json()
    except BaseException as e:
        return str(e)
    return r


@gen_sig
def post_(path, data, expires, verb, headers):
    '''
    sends get post request to Bitmex API.
    must include quantity and symbol
    :param path: path to API resource.
    :param headers: is set by the @gen_sig.
    :param data: for sending data to server.
    '''
    try:
        r = requests.post(URL + f'{path}', data, headers=headers).json()
    except BaseException as e:
        return str(e)
    return r


def get_wallet():
    '''
    details at /api/explorer/#/Position
    '''
    r = get_(path=f'/api/v1/user/wallet?currency=XBt',
             expires=str(pendulum.now('UTC').int_timestamp + 10),
             data='',
             verb='GET',)
    return r['amount']


def get_current_position():
    '''
    details at /api/explorer/#/Position
    return current position.
    '''
    filter = quote('{"symbol": "XBTUSD"}', safe='')
    try:
        _r = get_(path=f'/api/v1/position?filter={filter}&columns=currentQty',
                expires=str(pendulum.now('UTC').int_timestamp + 10),
                data='',
                verb='GET')
        r = _r[0]['currentQty']
        if r > 0:
            return 'Long'
        if r < 0:
            return 'Short'
        else:
            return None
    except BaseException as e:
        return str(e)


def update_position(side, order=None, amt=None):
    '''
    Execute sell or buy, marketorder
    '''
    data = {"symbol":   "XBTUSD",
            "side":     side,
            "ordType":  "Market"}
    if amt:
        data.update({'simpleOrderQty': amt})
    if order:
        data.update({"execInst": order})
    try:
        fill = post_(path='/api/v1/order',
                     expires=str(pendulum.now('UTC').int_timestamp + 10),
                     data=dumps(data),
                     verb='POST')
        fill_values = tuple(fill.values())
        _fill = [fill_values[i] for i in [0, 5, 27, 31]]
    except Exception as err:
        return str(err)
    return _fill


def add_leverage(leverage):
    '''
    Add leverage based on int in setting file
    details at api/explorer/#!/Position/Position_updateLeverage
    '''
    data = {"symbol":   "XBTUSD",
            "leverage": leverage}
    try:
        fill = post_(path='/api/v1/position/leverage',
                     expires=str(pendulum.now('UTC').int_timestamp + 10),
                     data=dumps(data),
                     verb='POST')
    except Exception as err:
        return str(err)


def exit_side():
        '''
        returns transaction info from bitmex
        '''
        current_position = get_current_position()
        close_side = alert_bin['exit_side'].get(current_position)
        close_info = update_position(side=close_side, order='Close')
        return close_info


def open_side(alert):
        '''
        returns transaction info from bitmex
        '''
        open_side = alert_bin['entry_side'].get(alert)
        acct_balance = get_wallet()
        _amt = (acct_balance/100000000) * TRADE_SIZE_PERCENT
        amt = float(round(_amt, 3) * 25)
        open_info = update_position(side=open_side, amt=amt)
        lev_info = add_leverage(leverage=LEVERAGE)
        return open_info


def trade(alert):
    '''
    Closes and opens positions. If alert is strict, position is simply
    closed. If alert is smart a two step routine is performed, which consist
    of closing the position and opening a new position using parameters
    found in the setting file.
    Returns a list [transid, ordertype, amount, timeexecuted]

    '''

    if (alert in alert_bin['strict_exit']) & \
       (get_current_position() is not None):
        close_info = exit_side()
        mark_msg_as_read()
        return {'close': close_info}

    if (alert in alert_bin['smart_exit']) & \
       (get_current_position() is not None):
        close_info = exit_side()
        open_info = open_side(alert)
        mark_msg_as_read()
        return {'open': open_info, 'close': close_info}

    if (alert in alert_bin['smart_exit']) & \
       (get_current_position() is None):
        open_info = open_side(alert)
        mark_msg_as_read()
        return {'open': open_info, 'leverage': '', 'open_side': ''}

    else:
        return None
